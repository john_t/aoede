{
  description = "A lovely little terminal music player";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";

    crane = {
      url = "github:ipetkov/crane";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, crane, flake-utils, ... }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = nixpkgs.legacyPackages.${system};

        craneLib = crane.mkLib pkgs;

        aoede-crate = craneLib.buildPackage {
          src = craneLib.cleanCargoSource (craneLib.path ./.);
          strictDeps = true;

          OPENSSL_NO_VENDOR = 1;
          OPENSSL_DEV = pkgs.openssl.dev;

          nativeBuildInputs = with pkgs; [ pkg-config makeWrapper ];

          buildInputs = with pkgs; [ openssl ];

          propagatedBuildInputs = with pkgs.gst_all_1;
            with pkgs;
            [
              glib
              gstreamer
              gst-plugins-base
              gst-plugins-good
              gst-plugins-bad
              gst-plugins-ugly
              gst-libav
              # Add additional build inputs here
            ] ++ pkgs.lib.optionals pkgs.stdenv.isDarwin [
              # Additional darwin specific inputs can be set here
              pkgs.libiconv
            ];

          postInstall = ''
            wrapProgram "$out/bin/aoede" --set GST_PLUGIN_SYSTEM_PATH_1_0 "$GST_PLUGIN_SYSTEM_PATH_1_0"
          '';
        };
      in {
        checks = { my-crate = aoede-crate; };

        packages.default = aoede-crate;

        apps.default = flake-utils.lib.mkApp { drv = aoede-crate; };

        devShells.default = craneLib.devShell {
          # Inherit inputs from checks.
          checks = self.checks.${system};

          packages = with pkgs; [ git ];
        };
      }) // {
        homeManagerModules = {
          default = self.homeManagerModules.aoede;
          aoede = import ./nix/hm-module.nix self;
        };
      };
}
