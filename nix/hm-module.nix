self:
{ config, pkgs, lib, ... }:
with lib;

let
  cfg = config.programs.aoede;
  tomlFormat = pkgs.formats.toml { };
in {
  options.programs.aoede = {
    enable = mkEnableOption "Aoede";
    package = mkOption {
      type = types.package;
      default = pkgs.aoede;
      defaultText = literalExpression "pkgs.aoede";
      description = "Package providing the aoede tool.";
    };

    settings = mkOption {
      type = tomlFormat.type;
      default = { };
    };
  };

  config = mkIf cfg.enable {
    home.packages = [ cfg.package ];
    xdg.configFile."aoede/config.toml".source =
      tomlFormat.generate "config.toml" cfg.settings;
  };
}
