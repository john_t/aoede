use anyhow::Result;
use dirs::cache_dir;
use image::io::Reader as ImageReader;
use std::{
    collections::{hash_map::DefaultHasher, HashMap},
    hash::{Hash, Hasher},
    io::Cursor,
    path::{Path, PathBuf},
};

use anyhow::Context;
use futures::StreamExt;
use image::DynamicImage;
use reqwest::Client;
use rss::{Channel, Item};
use slab::Slab;
use tokio::{
    fs::{self, File},
    io::AsyncWriteExt as _,
    sync::mpsc::Sender,
};
use url::Url;

use crate::track::podcast::ItemRef;

#[derive(Default)]
pub struct ResManager {
    pub local_images: HashMap<PathBuf, DynamicImage>,
    pub downloaded_images: HashMap<Url, PathBuf>,
    pub rss_channels: Slab<Channel>,
    pub client: Client,
}

impl ResManager {
    pub fn new() -> Self {
        Self::default()
    }
    pub fn register_art(&mut self, p: impl Into<PathBuf>, image: DynamicImage) {
        self.local_images.insert(p.into(), image);
    }

    pub fn rss_item<'a>(&'a self, key: &ItemRef) -> Option<&'a Item> {
        self.rss_channels
            .get(key.channel)
            .and_then(|x| x.items.get(key.item))
    }

    pub fn rss_item_with_channel<'a>(
        &'a self,
        key: &ItemRef,
    ) -> Option<(&'a Channel, &'a Item)> {
        let channel = self.rss_channels.get(key.channel);
        channel.and_then(|channel| {
            channel.items.get(key.item).map(|x| (channel, x))
        })
    }

    pub fn suggest_album_art<'a>(
        &'a self,
        p: &Path,
    ) -> Option<&'a DynamicImage> {
        let mut suggestion = None;
        let mut suggestion_depth = 0;
        for (path, image) in &self.local_images {
            if p.starts_with(path) {
                let depth = path.components().count();
                if depth >= suggestion_depth {
                    suggestion = Some(image);
                    suggestion_depth = depth;
                }
            }
        }
        suggestion
    }

    pub fn suggest_album_art_for_url<'a>(
        &'a self,
        url: &Url,
    ) -> Option<&'a DynamicImage> {
        self.downloaded_images
            .get(url)
            .and_then(|path| self.local_images.get(path))
    }

    pub fn is_empty(&self) -> bool {
        self.local_images.is_empty()
    }

    pub async fn ensure_image_downloaded<'a>(
        &'a mut self,
        url: &Url,
        sender: Sender<ImageLoaded>,
    ) {
        if !self.downloaded_images.contains_key(url) {
            let url = url.clone();
            let client = self.client.clone();
            tokio::spawn(async move {
                let path = cache_file_from_url(&url)?;
                let cache_dir =
                    dirs::cache_dir().context("no cache dir")?.join("aoede/");

                let in_memory = if let Ok(file) = fs::read(&path).await {
                    file
                } else {
                    let body = client.get(url.clone()).send().await?;
                    let capacity = body.content_length().unwrap_or(0) as usize;
                    let mut body_stream = body.bytes_stream();
                    let body_stream = body_stream.by_ref();

                    fs::create_dir_all(cache_dir).await?;
                    let mut file = fs::OpenOptions::new()
                        .write(true)
                        .create(true)
                        .open(&path)
                        .await
                        .unwrap();

                    let mut in_memory = Vec::with_capacity(capacity);

                    while let Some(v) = body_stream.next().await {
                        let v = v?;
                        file.write_all(&v).await.unwrap();
                        in_memory.extend_from_slice(&v);
                    }
                    in_memory
                };
                let reader = ImageReader::new(Cursor::new(in_memory))
                    .with_guessed_format()
                    .expect("Cursor io never fails");
                sender
                    .send(ImageLoaded {
                        url,
                        image: reader.decode()?,
                    })
                    .await?;
                Ok::<(), anyhow::Error>(())
            });
        }
    }

    pub fn register_loaded_image(
        &mut self,
        msg: ImageLoaded,
    ) -> anyhow::Result<()> {
        let path = cache_file_from_url(&msg.url)?;
        self.downloaded_images.insert(msg.url, path.clone());
        self.local_images.insert(path, msg.image);
        Ok(())
    }
}

fn cache_file_from_url(url: &Url) -> Result<PathBuf> {
    let cache_dir = dirs::cache_dir().context("no cache dir")?.join("aoede/");
    let filename = url
        .path_segments()
        .context("no path in url")?
        .last()
        .context("no path in url")?;
    let mut hasher = DefaultHasher::new();
    url.hash(&mut hasher);
    let hash = hasher.finish();
    Ok(cache_dir.join(format!("{hash}_{filename}")))
}
pub struct ImageLoaded {
    pub url: Url,
    pub image: DynamicImage,
}
