use async_recursion::async_recursion;
use rss::Channel;
use std::{
    fmt::{self, Display},
    io::{BufReader, Cursor},
    path::Path,
    str::FromStr,
    time::Duration,
};

use anyhow::{anyhow, bail, Result};
use dirs::config_dir;
use futures::{Stream, StreamExt};
use gst::ClockTime;
use gst_play::{Play, PlayMessage, PlayVideoRenderer};
use image::io::Reader as ImageReader;
use tokio::{
    fs,
    sync::mpsc::{channel, Receiver, Sender},
    time::{interval, Interval},
};
use tuviv::{
    le::{layout::Rect, Alignment, Orientation},
    prelude::{StylableExt, WidgetExt},
    widgets::{Flexbox, Paragraph, ProgressBar},
    Modifier, Widget,
};
use url::Url;

use crate::{
    config::Config,
    mpris::{Mpris, MprisCreation, MprisInterface, Msg, SkelatonTrack},
    resource_manager::{ImageLoaded, ResManager},
    track::{
        local::LocalTrack,
        podcast::{ItemRef, PodcastTrack},
        Track, TrackWidgetState,
    },
};

#[derive(Copy, Clone, Debug)]
pub enum LoopStatus {
    None,
    Track,
    Playlist,
}

impl Display for LoopStatus {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let x = match self {
            Self::None => "None",
            Self::Track => "Track",
            Self::Playlist => "Playlist",
        };
        write!(f, "{x}")
    }
}

impl FromStr for LoopStatus {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "None" => Ok(Self::None),
            "Track" => Ok(Self::Track),
            "Playlist" => Ok(Self::Playlist),
            _ => Err(()),
        }
    }
}

pub struct State {
    pub tracks: Vec<Track>,
    track: Option<usize>,
    selected_track: usize,
    play: Play,
    paused: bool,
    res_manager: ResManager,
    volume: u8,
    config: Config,
    loop_status: LoopStatus,
    mpris_sender: Sender<Msg>,
    mpris_recv: Receiver<Msg>,
    image_recv: Receiver<ImageLoaded>,
    image_sender: Sender<ImageLoaded>,
    must_send_skelaton_track: bool,
}

impl State {
    pub async fn new() -> anyhow::Result<Self> {
        // Create the play
        let play = Play::new(None::<PlayVideoRenderer>);
        play.set_volume(0.5);

        // Get the config file
        let config_dir = config_dir();
        let config = match config_dir {
            Some(mut x) => {
                x.push("aoede/config.toml");
                match fs::read_to_string(x).await {
                    Ok(f) => toml::from_str(&f)?,
                    Err(_) => Config::default(),
                }
            }
            None => Config::default(),
        };

        let creation = MprisCreation::new();
        let mpris = creation.mpris;
        let mpris_sender = creation.mt_sender;
        let mpris_recv = creation.mt_recv;
        let mut mpris_thread_recv = creation.mp_recv;
        let done_listener = mpris.done.listen();

        tokio::task::spawn(async move {
            let _inner = mpris.inner.clone();

            let conn = zbus::Connection::session().await.unwrap();
            conn.object_server()
                .at("/org/mpris/MediaPlayer2", mpris)
                .await
                .unwrap();
            conn.object_server()
                .at("/org/mpris/MediaPlayer2", MprisInterface)
                .await
                .unwrap();
            conn.request_name("org.mpris.MediaPlayer2.aoede")
                .await
                .unwrap();

            let _signal_context = conn
                .object_server()
                .interface::<_, Mpris>("/org/mpris/MediaPlayer2")
                .await
                .unwrap();

            tokio::select!(
                _ = done_listener => (),
                _ = async {
                    loop {
                        let msg = mpris_thread_recv.recv().await;
                        if let Some(msg) = msg {
                            let iface_ref = conn.object_server()
                                .interface::<_, Mpris>("/org/mpris/MediaPlayer2").await.unwrap();
                            let iface = iface_ref.get_mut().await;
                            iface.handle(msg).await.unwrap();
                            iface.send_changed(iface_ref.signal_context()).await;
                        }
                    }
                } => (),
            )
        });

        let (image_sender, image_recv) = channel(8);

        Ok(Self {
            tracks: vec![],
            track: None,
            selected_track: 0,
            play,
            paused: false,
            res_manager: ResManager::new(),
            volume: 50,
            config,
            loop_status: LoopStatus::None,
            mpris_sender,
            mpris_recv,
            image_sender,
            image_recv,
            must_send_skelaton_track: false,
        })
    }

    pub async fn quit(&mut self) -> anyhow::Result<()> {
        tokio::join!(self.mpris_sender.send(Msg::Die), async {
            let _ = fs::remove_file("/tmp/aoede.png").await;
        },)
        .0?;
        Ok(())
    }

    pub async fn handle_messages(&mut self) -> anyhow::Result<()> {
        tokio::select! {
            Some(msg) = self.image_recv.recv() => {
                self.res_manager.register_loaded_image(msg)?;
            },
            Some(msg) = self.mpris_recv.recv() => {
                match msg {
                    Msg::Next => self.play_next_song().await?,
                    Msg::OpenUri(file) => {
                        self.add_file(&Url::parse(&file)?.to_file_path().map_err(
                            |_| {
                                anyhow!("failed to get a file path from mpris song")
                            },
                        )?)
                        .await?;
                    }
                    Msg::Pause => {
                        self.play.pause();
                        self.paused = true;
                    }
                    Msg::Play => {
                        self.play.play();
                        self.paused = false;
                    }
                    Msg::PlayPause => self.toggle_pause().await?,
                    Msg::Previous => self.play_prev_song().await?,
                    Msg::Seek(seek) => self.play.seek(seek.try_into()?),
                    Msg::Stop => {
                        self.play.pause();
                        self.paused = true;
                    }
                    Msg::SetSkelatonTrack(_) => bail!("Skelaton track"),
                    Msg::SetShuffle(_) => todo!(),
                    Msg::SetVolume(vol) => self.set_volume(vol).await?,
                    Msg::SetLoopStatus(_) => todo!(),
                    Msg::Die => bail!("Ayo what, mpris just tell big man go die"),
                }
            }
        }

        Ok(())
    }

    pub fn sort_tracks(&mut self) {
        self.tracks.sort();
    }

    pub fn get_stream(&self) -> impl Stream<Item = PlayMessage> {
        self.play
            .message_bus()
            .stream()
            .map(|x| PlayMessage::parse(&x).unwrap())
            .filter(|x| {
                futures::future::ready(matches!(x, PlayMessage::EndOfStream))
            })
    }

    pub async fn play_selected(&mut self) -> anyhow::Result<()> {
        self.track = Some(self.selected_track);
        if let Some(Track::Podcast(track)) = &self.track() {
            if let Some(url) = track.image_url(&self.res_manager) {
                self.res_manager
                    .ensure_image_downloaded(&url, self.image_sender.clone())
                    .await;
            }
        }
        self.play().await
    }

    async fn play(&mut self) -> Result<()> {
        if let Some(track) = self.track() {
            self.play
                .set_uri(Some(track.url(&self.res_manager).as_str()));
            self.play.play();

            // Write the image
            if let Some(track) = self.track() {
                if let Some(image) = &track.image(&self.res_manager) {
                    image.save("/tmp/aoede.png")?;
                }
            }
        } else {
            self.play.stop();
            self.track = None;
        }
        self.must_send_skelaton_track = true;

        Ok(())
    }

    pub async fn play_next_song(&mut self) -> anyhow::Result<()> {
        self.track = self.track.map(|x| x + 1);
        self.play().await
    }

    pub async fn play_prev_song(&mut self) -> anyhow::Result<()> {
        self.track = self.track.map(|x| x - 1);
        self.play().await
    }

    pub fn stop(&mut self) {
        self.play.stop();
    }

    pub fn track_n(&self) -> Option<usize> {
        self.track
    }

    pub fn select_next_track(&mut self) {
        self.selected_track += 1;
        self.selected_track =
            self.selected_track.min(self.tracks.len().saturating_sub(1));
    }

    pub fn select_prev_track(&mut self) {
        self.selected_track = self.selected_track.saturating_sub(1);
    }

    pub fn track(&self) -> Option<&Track> {
        self.track.and_then(|track| self.tracks.get(track))
    }

    pub async fn toggle_pause(&mut self) -> anyhow::Result<()> {
        self.paused ^= true;
        if self.paused {
            self.play.pause();
            self.mpris_sender.send(Msg::Pause).await?;
        } else {
            self.play.play();
            self.mpris_sender.send(Msg::Play).await?;
        }

        Ok(())
    }

    /// Adds a file to this
    #[async_recursion(?Send)]
    pub async fn add_file(&mut self, path: &Path) -> anyhow::Result<()> {
        if path.is_dir() {
            self.add_dir(path).await
        } else {
            // Ick!
            if path.extension().map(|x| x == "rss").unwrap_or(false) {
                self.add_rss(path).await?;
                Ok(())
            } else {
                // See if we can open it as a track
                let track = LocalTrack::new(path.to_owned()).await;
                match track {
                    Ok(track) => {
                        self.tracks.push(track.into());
                        return Ok(());
                    }
                    Err(e) => {
                        log::info!(
                            "file {} is not a track {}",
                            path.display(),
                            e
                        )
                    }
                }

                // Try to open an image
                let image = ImageReader::open(path);
                if let Ok(image) = image {
                    let image = image.decode();
                    if let Ok(image) = image {
                        self.res_manager
                            .register_art(path.parent().unwrap(), image);
                        return Ok(());
                    }
                }

                bail!("Unrecognised file type: {}", path.display());
            }
        }
    }

    pub async fn add_rss(&mut self, path: &Path) -> anyhow::Result<()> {
        let file = fs::read(path).await?;
        let file = Cursor::new(file);
        let channel = Channel::read_from(BufReader::new(file)).unwrap();
        // let link = channel.link;
        // panic!("{}", link);
        // let content = reqwest::get(&link).await?.bytes().await?;
        // fs::write(path, &content).await?;
        // let content = BufReader::new(Cursor::new(content));
        // let channel = Channel::read_from(content).unwrap();

        let channel_ref = self.res_manager.rss_channels.insert(channel);
        let channel = &self.res_manager.rss_channels[channel_ref];

        for (item_n, item) in channel.items.iter().enumerate() {
            let item_ref = ItemRef {
                channel: channel_ref,
                item: item_n,
            };
            self.tracks
                .push(Track::from(PodcastTrack::new(item_ref, item)))
        }

        Ok(())
    }

    /// Force cover art
    pub fn force_art(&mut self) -> anyhow::Result<()> {
        if self.res_manager.is_empty() {
            for track in &mut self.tracks {
                if let Track::Local(track) = track {
                    if track.image.is_none() {
                        track.generate_track_image()?;
                    }
                }
            }
        }

        Ok(())
    }

    pub async fn add_dir(&mut self, path: &Path) -> anyhow::Result<()> {
        let mut dir = fs::read_dir(path).await?;
        while let Some(item) = dir.next_entry().await? {
            self.add_file(&item.path()).await?;
        }

        Ok(())
    }

    pub async fn change_volume(&mut self, volume: i8) -> Result<()> {
        if volume > 0 {
            self.set_volume(self.volume + volume.unsigned_abs()).await
        } else {
            self.set_volume(self.volume - volume.unsigned_abs()).await
        }
    }

    pub async fn set_volume(&mut self, volume: u8) -> Result<()> {
        self.volume = volume;

        self.mpris_sender.send(Msg::SetVolume(self.volume)).await?;
        let vol = self.volume as f64 / 100.0;
        let vol = vol.clamp(0.0, 1.0);
        self.play.set_volume(vol);

        Ok(())
    }

    pub async fn scrub(&mut self, time: f64) -> Result<()> {
        let position = self.play.position().unwrap_or_default().seconds_f64();
        let position = (position + time).max(0.0);
        let position = ClockTime::from_seconds_f64(position);
        self.play.seek(position);

        self.mpris_sender
            .send(Msg::Seek(self.play.position().unwrap_or_default().into()))
            .await?;

        Ok(())
    }

    pub fn get_interval(&self, size_x: u16) -> Interval {
        let duration = self
            .play
            .duration()
            .unwrap_or(ClockTime::from_seconds(100))
            .mseconds() as f64
            / 1000.0;
        let dur = Duration::from_secs_f64(duration / (size_x as f64));
        interval(dur / 8)
    }

    pub fn info_widget(&self) -> impl Widget {
        let duration = self.play.duration().unwrap_or_default().seconds();
        let position = self.play.position().unwrap_or_default().seconds();
        let dur_m = duration / 60;
        let dur_s = duration % 60;
        let pos_m = position / 60;
        let pos_s = position % 60;
        let vol = self.volume;

        let total_duration = self.total_duration();
        let total_duration_h = total_duration.as_secs() / 3600;
        let total_duration_m = total_duration.as_secs() / 60 % 60;
        let total_duration_s = total_duration.as_secs() % 60;

        let dur_text = if total_duration_h != 0 {
            format!("{pos_m}:{pos_s:02} / {dur_m}:{dur_s:02}  = {total_duration_h}:{total_duration_m:02}:{total_duration_s:02}")
        } else {
            format!("{pos_m}:{pos_s:02} / {dur_m}:{dur_s:02}  = {total_duration_m}:{total_duration_s:02}")
        }
        .styled();
        Flexbox::new(Orientation::Vertical, false)
            .child(Paragraph::label(dur_text).into())
            .child(Paragraph::label(format!("vol: {vol}%").styled()).into())
    }

    pub fn sidebar_widget(&self) -> impl Widget {
        let mut track_fbx = Flexbox::new(Orientation::Vertical, false);

        // Add the tracks
        for (i, track) in self.tracks.iter().enumerate() {
            let playing = Some(i) == self.track;
            let selected = i == self.selected_track;
            let widget_state = TrackWidgetState {
                selected,
                playing,
                paused: self.paused,
                config: &self.config,
                res_manager: &self.res_manager,
            };
            track_fbx
                .children
                .push(track.widget(&widget_state).to_flex_child());
        }

        let mut f = Flexbox::new(Orientation::Vertical, false).child(
            track_fbx
                .stacked_background(self.config.styles.sidebar.bg)
                .to_scroll()
                .scroll_y(true)
                .scroll_x(true)
                .focus_position(Rect::new(1, self.selected_track, 1, 3))
                .to_flex_child()
                .expand(1),
        );

        // Add the info widget
        f.children
            .push(self.info_widget().align_y(Alignment::End).to_flex_child());

        // Give it a backing
        let stack = f.stacked_background(self.config.styles.sidebar.bg);
        stack.constrained_size(20..40, ..)
    }

    pub fn progress_widget(&self) -> impl Widget {
        let duration = self.play.duration().unwrap_or_default().mseconds();
        let duration = duration as f64 / 1000.0;
        let position = self.play.position().unwrap_or_default().mseconds();
        let mut position = position as f64 / 1000.0;
        if duration == 0.0 {
            position = 0.0;
        }

        let mut fg_style = self.config.styles.progress_fg;
        let edge_style = self.config.styles.progress_fg;
        fg_style.add_modifier ^= Modifier::REVERSED;

        ProgressBar::new()
            .total(duration)
            .value(position)
            .fg(vec![" ".with_style(fg_style)])
            .bg(vec![" ".with_style(edge_style)])
            .edge([
                " ".with_style(edge_style),
                "▏".with_style(edge_style),
                "▎".with_style(edge_style),
                "▍".with_style(edge_style),
                "▌".with_style(edge_style),
                "▋".with_style(edge_style),
                "▊".with_style(edge_style),
                "▉".with_style(edge_style),
            ])
    }

    pub async fn check_send_skelaton_track(&mut self) -> anyhow::Result<()> {
        let mut must_send_skelaton_track = self.must_send_skelaton_track;
        if must_send_skelaton_track {
            if let Some(track) = self.track() {
                if let Some(duration) = self.play.duration() {
                    self.mpris_sender
                        .send(Msg::SetSkelatonTrack(SkelatonTrack {
                            url: track.url(&self.res_manager),
                            metadata: track.tags(&self.res_manager),
                            length: duration.into(),
                        }))
                        .await?;
                    must_send_skelaton_track = false;
                }
            }
        }

        self.must_send_skelaton_track = must_send_skelaton_track;

        Ok(())
    }

    pub fn large_widget(&self) -> Option<impl Widget + '_> {
        self.track()
            .map(|x| x.large_widget(&self.res_manager, &self.config))
    }

    fn total_duration(&self) -> Duration {
        self.tracks
            .iter()
            .map(|x| x.duration().unwrap_or_default())
            .sum()
    }
}
