// This example shows how to use the GstPlay API.
// The GstPlay API is a convenience API to allow implement playback applications
// without having to write too much code.
// Most of the tasks a play needs to support (such as seeking and switching
// audio / subtitle streams or changing the volume) are all supported by simple
// one-line function calls on the GstPlay.

use std::{path::PathBuf, time::Duration};

use anyhow::Error;
use tokio_stream::StreamExt;
mod state;
use crossterm::{
    event::{Event, EventStream, KeyCode},
    terminal,
};
use state::State;
mod mpris;
mod resource_manager;
mod track;

mod config;

use clap::Parser;
use gst_play::PlayMessage;
use tuviv::{
    le::{grid::Sizing, Alignment},
    prelude::WidgetExt,
    widgets::Grid,
};

#[derive(Parser, Debug)]
#[command(author, version, about)]
struct Args {
    /// List of files
    files: Vec<PathBuf>,
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    env_logger::init();
    let args = Args::parse();
    gst::init()?;

    // Create the state
    let mut state = State::new().await?;
    for file in &args.files {
        state.add_file(file).await?;
    }

    if args.files.is_empty() {
        let path = std::env::current_dir()?;
        state.add_dir(&path).await?;
    }

    state.force_art()?;

    state.sort_tracks();

    // Create the app
    let mut app = tuviv::App::new()?;

    let mut size = terminal::size()?;

    let mut event_stream = EventStream::new();
    let mut stream = state.get_stream();
    let mut interval = tokio::time::interval(Duration::from_secs_f32(0.1));
    let mut first = true;

    loop {
        let event = if first {
            None
        } else {
            tokio::select! {
                v = event_stream.next() => v,
                _ = interval.tick() => {
                    // interval = state.get_interval(size.0);
                    None
                },
                _ = state.handle_messages() => None,

                // Move onto the next track when finished
                msg = stream.next() => {
                    if let Some(PlayMessage::EndOfStream) = msg {
                        if state.track_n().map(|x| x + 1) == Some(state.tracks.len()) {
                            state.stop()
                        } else {
                            state.play_next_song().await?;
                        }
                        interval = state.get_interval(size.0);
                    }
                    None
                }
            }
        }
        .transpose()?;
        first = false;

        if let Some(Event::Key(key)) = event {
            match key.code {
                KeyCode::Char('q') => break,
                KeyCode::Char('j') | KeyCode::Down => {
                    state.select_next_track();
                }
                KeyCode::Char('k') | KeyCode::Up => {
                    state.select_prev_track();
                }
                KeyCode::Char('h') | KeyCode::Left => state.scrub(-5.0).await?,
                KeyCode::Char('l') | KeyCode::Right => state.scrub(5.0).await?,
                KeyCode::Char('+') => state.change_volume(5).await?,
                KeyCode::Char('-') => state.change_volume(-5).await?,
                KeyCode::Enter => {
                    state.play_selected().await?;
                }
                KeyCode::Char(' ') => {
                    state.toggle_pause().await?;
                }
                _ => (),
            }
        } else if let Some(Event::Resize(x, y)) = event {
            size.0 = x;
            size.1 = y;
        }

        state.check_send_skelaton_track().await?;

        let sidebar = state.sidebar_widget().align_x(Alignment::Begin);
        let progress = state.progress_widget();
        let mut grid = Grid::new()
            .template_row(Sizing::Fractional(1))
            .template_row(Sizing::Auto)
            .template_column(Sizing::Auto)
            .template_column(Sizing::Fractional(1))
            .child(sidebar.to_grid_child().row(0).column(0))
            .child(progress.to_grid_child().row(1).column(0).column_span(2));

        // Add the large widget of the current track if available
        if let Some(large_widget) = state.large_widget() {
            grid = grid.child(large_widget.to_grid_child().row(0).column(1));
        }

        app.render_widget(grid)?;
    }

    state.quit().await?;

    Ok(())
}
