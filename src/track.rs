use either::Either;
use lofty::{Accessor as _, TaggedFileExt as _};
use std::{
    borrow::Cow,
    cmp::{Ordering, Reverse},
    time::Duration,
};
use url::Url;

use image::DynamicImage;
use local::LocalTrack;
use podcast::PodcastTrack;
use tuviv::{
    le::Orientation,
    prelude::*,
    widgets::{cache::CachedBuffers, Flexbox, KittyImage, Paragraph},
    Style, Widget,
};

use crate::{config::Config, mpris::Tags, resource_manager::ResManager};

pub mod local;
pub mod podcast;

pub enum Track {
    Local(LocalTrack),
    Podcast(PodcastTrack),
}

impl Track {
    pub fn tags(&self, res_manager: &ResManager) -> Tags {
        match self {
            Track::Local(x) => x.tags(),
            Track::Podcast(x) => x.tags(res_manager),
        }
    }

    pub fn url(&self, res_manager: &ResManager) -> Url {
        match self {
            Track::Local(local) => local.uri.clone(),
            Track::Podcast(podcast) => {
                let item = res_manager.rss_item(&podcast.item_ref);
                Url::parse(
                    &item.as_ref().unwrap().enclosure.as_ref().unwrap().url,
                )
                .unwrap()
            }
        }
    }
    pub fn widget(&self, state: &TrackWidgetState<'_>) -> impl Widget {
        match self {
            Track::Local(x) => Either::Left(x.widget(state)),
            Track::Podcast(x) => Either::Right(x.widget(state)),
        }
    }
    pub fn large_widget<'a>(
        &'a self,
        res_manager: &'a ResManager,
        config: &Config,
    ) -> impl Widget + 'a {
        match self {
            Track::Local(x) => {
                Either::Left(x.large_widget(res_manager, config))
            }
            Track::Podcast(x) => {
                Either::Right(x.large_widget(res_manager, config))
            }
        }
    }

    pub fn duration(&self) -> Option<Duration> {
        match self {
            Track::Local(x) => x
                .info
                .as_ref()
                .and_then(|x| x.duration())
                .map(Duration::from),
            Track::Podcast(_) => None,
        }
    }

    pub fn image<'a>(
        &'a self,
        res_manager: &'a ResManager,
    ) -> Option<&'a DynamicImage> {
        match self {
            Track::Local(x) => x.image(res_manager),
            Track::Podcast(x) => x.image(res_manager),
        }
    }

    fn sort_key(&self) -> impl Ord + '_ {
        match self {
            Track::Local(x) => {
                let tag = x.metadata.primary_tag();
                Either::Left((
                    tag.and_then(|x| x.album()),
                    tag.and_then(|x| x.track()),
                    &x.path,
                ))
            }
            Track::Podcast(x) => Either::Right(x.item_ref),
        }
    }
}

pub struct TrackWidgetState<'a> {
    pub selected: bool,
    pub playing: bool,
    pub paused: bool,
    pub config: &'a Config,
    pub res_manager: &'a ResManager,
}

impl TrackWidgetState<'_> {
    fn style(&self) -> Style {
        self.style_with_default(Style::default())
    }

    fn style_with_default(&self, default: Style) -> Style {
        if self.selected {
            self.config.styles.sidebar_selected
        } else if self.playing {
            if self.paused {
                self.config.styles.sidebar_paused
            } else {
                self.config.styles.sidebar_playing
            }
        } else {
            default
        }
    }

    fn name_prepender(&self) -> &str {
        if self.playing {
            if self.paused {
                &self.config.pause
            } else {
                &self.config.play
            }
        } else {
            "  "
        }
    }
}

impl PartialEq for Track {
    fn eq(&self, other: &Self) -> bool {
        self.sort_key().eq(&other.sort_key())
    }
}

impl Eq for Track {}

impl PartialOrd for Track {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl Ord for Track {
    fn cmp(&self, other: &Self) -> Ordering {
        self.sort_key().cmp(&other.sort_key())
    }
}

#[derive(Default)]
pub struct SkeletalTags<'a> {
    pub image: Option<KittyImage<'a, DynamicImage>>,
    pub title: Option<Cow<'a, str>>,
    pub album: Option<Cow<'a, str>>,
    pub artist: Option<Cow<'a, str>>,
}

fn create_large_widget<'a>(
    tags: SkeletalTags<'a>,
    image_cache: &CachedBuffers,
    config: &Config,
) -> impl Widget + 'a {
    let mut f = Flexbox::new(Orientation::Vertical, false);
    if let Some(image) = tags.image {
        f.children.push(
            image
                .constrained_size(..30, ..15)
                .cached(image_cache)
                .max_capacity(1)
                .centered()
                .to_flex_child(),
        );
    }

    if let Some(title) = tags.title {
        f.children.push(
            Paragraph::new(title.with_style(config.styles.track_title))
                .constrained_size(..30, ..)
                .to_flex_child(),
        );
    }

    if let Some(album) = tags.album {
        f.children.push(
            Paragraph::new(album.with_style(config.styles.track_album))
                .constrained_size(..30, ..)
                .to_flex_child(),
        );
    }

    if let Some(artist) = tags.artist {
        f.children.push(
            Paragraph::new(artist.with_style(config.styles.track_artist))
                .constrained_size(..30, ..)
                .to_flex_child(),
        );
    }

    f.centered()
}

impl From<LocalTrack> for Track {
    fn from(value: LocalTrack) -> Self {
        Self::Local(value)
    }
}
impl From<PodcastTrack> for Track {
    fn from(value: PodcastTrack) -> Self {
        Self::Podcast(value)
    }
}
