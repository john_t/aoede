use serde::{Deserialize, Serialize};
use tuviv::Style;

#[derive(Serialize, Deserialize)]
pub struct Config {
    pub styles: Styles,

    pub play: String,
    pub pause: String,
}

impl Default for Config {
    fn default() -> Self {
        Self {
            styles: Default::default(),
            play: String::from("🞂 "),
            pause: String::from(r#"" "#),
        }
    }
}

#[derive(Serialize, Deserialize)]
pub struct Styles {
    #[serde(default)]
    pub sidebar: Style,
    #[serde(default)]
    pub sidebar_selected: Style,
    #[serde(default)]
    pub sidebar_playing: Style,
    #[serde(default)]
    pub sidebar_paused: Style,
    #[serde(default)]
    pub sidebar_new: Style,

    #[serde(default)]
    pub progress_fg: Style,

    #[serde(default)]
    pub track_title: Style,
    #[serde(default)]
    pub track_artist: Style,
    #[serde(default)]
    pub track_album: Style,
}

impl Default for Styles {
    fn default() -> Self {
        Self {
            sidebar: Style::default().reversed(),
            sidebar_selected: Style::default().yellow(),
            sidebar_playing: Style::default().yellow(),
            sidebar_paused: Style::default().yellow(),
            sidebar_new: Style::default().yellow().bold(),

            progress_fg: Style::default().yellow(),

            track_title: Style::default().bold(),
            track_artist: Style::default(),
            track_album: Style::default(),
        }
    }
}
