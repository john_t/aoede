use image::imageops::FilterType;
use std::{
    borrow::Cow,
    collections::hash_map::DefaultHasher,
    hash::{Hash as _, Hasher as _},
    num::NonZeroU32,
};
use tuviv::{
    prelude::*,
    widgets::{cache::CachedBuffers, KittyImage},
    Style,
};

use tuviv::{widgets::Paragraph, Widget};
use url::Url;

use crate::{config::Config, mpris::Tags, resource_manager::ResManager};

use super::{create_large_widget, SkeletalTags, TrackWidgetState};

#[derive(Copy, Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct ItemRef {
    pub channel: usize,
    pub item: usize,
}

pub struct PodcastTrack {
    pub item_ref: ItemRef,
    pub new: bool,
    pub id: NonZeroU32,
    pub image_cache: CachedBuffers,
}

impl PodcastTrack {
    pub(crate) fn new(item_ref: ItemRef, item: &rss::Item) -> Self {
        let mut s = DefaultHasher::new();
        item.link.hash(&mut s);
        let id = NonZeroU32::new((s.finish() as u32 % 244) + 1).unwrap();

        Self {
            item_ref,
            new: false,
            id,
            image_cache: Default::default(),
        }
    }

    pub fn widget(&self, state: &TrackWidgetState<'_>) -> impl Widget {
        let default_style = if self.new {
            state.config.styles.sidebar_new
        } else {
            Style::default()
        };
        let style = state.style_with_default(default_style);

        let item = state.res_manager.rss_item(&self.item_ref).unwrap();
        let mut name = item.title().unwrap_or("<unknown>").to_owned();
        name.insert_str(0, state.name_prepender());

        Paragraph::label(name.with_style(style))
    }

    pub fn large_widget<'a>(
        &'a self,
        res_manager: &'a ResManager,
        config: &Config,
    ) -> impl Widget + 'a {
        let mut skeletal_tags = SkeletalTags::default();
        let (channel, item) =
            res_manager.rss_item_with_channel(&self.item_ref).unwrap();

        skeletal_tags.image = self.image(res_manager).map(|image| {
            // Don't use a filter for generated art
            let filter = FilterType::Lanczos3;
            KittyImage::new(image, self.id)
                .unwrap()
                .resize_filter(filter)
        });

        skeletal_tags.title = item.title().map(Cow::from);
        skeletal_tags.album = Some(Cow::from(channel.title()));

        create_large_widget(skeletal_tags, &self.image_cache, config)
    }

    pub fn tags(&self, res_manager: &ResManager) -> crate::mpris::Tags {
        let (channel, item) =
            res_manager.rss_item_with_channel(&self.item_ref).unwrap();
        Tags {
            title: item.title().map(|x| x.to_owned()),
            album: Some(channel.title().to_owned()),
            artist: None,
            genre: None,
            track_number: None,
        }
    }

    pub fn image<'a>(
        &self,
        res_manager: &'a ResManager,
    ) -> Option<&'a image::DynamicImage> {
        let image_url = self.image_url(res_manager);

        image_url.and_then(|image_url| {
            res_manager.suggest_album_art_for_url(&image_url)
        })
    }

    pub fn image_url(&self, res_manager: &ResManager) -> Option<Url> {
        let (channel, item) =
            res_manager.rss_item_with_channel(&self.item_ref).unwrap();

        let image_url: Option<Url> = item
            .itunes_ext
            .as_ref()
            .and_then(|x| x.image())
            .or(channel.image().map(|x| x.url()))
            .and_then(|x| x.parse().ok());
        image_url
    }
}
