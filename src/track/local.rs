use enclose::enc;
use std::collections::hash_map::DefaultHasher;
use std::fs::{self, canonicalize};
use std::hash::{Hash as _, Hasher as _};
use std::io::{Cursor, Read, Seek, SeekFrom};
use std::num::NonZeroU32;
use std::path::PathBuf;
use tuviv::prelude::StylableExt as _;

use anyhow::anyhow;
use gst::ClockTime;
use gst_pbutils::{Discoverer, DiscovererInfo};
use image::imageops::FilterType;
use image::io::Reader as ImageReader;
use image::{DynamicImage, ImageFormat, Rgb, RgbImage};
use lofty::{Accessor, PictureType, TaggedFile, TaggedFileExt};
use palette::convert::TryFromColor;
use palette::{FromColor, Hsv, Srgb};
use tokio::task::spawn_blocking;
use tuviv::widgets::KittyImage;
use tuviv::{
    widgets::{cache::CachedBuffers, Paragraph},
    Widget,
};
use url::Url;

use crate::config::Config;
use crate::mpris;
use crate::resource_manager::ResManager;

use super::{create_large_widget, SkeletalTags, TrackWidgetState};

pub struct LocalTrack {
    pub uri: Url,
    pub metadata: TaggedFile,
    pub image: Option<DynamicImage>,
    pub generated_image: bool,
    pub image_cache: CachedBuffers,
    pub path: PathBuf,
    pub info: Option<DiscovererInfo>,
    pub id: NonZeroU32,
}

impl LocalTrack {
    pub async fn new(path: PathBuf) -> Result<Self, anyhow::Error> {
        let path = canonicalize(path)?;
        let uri =
            Url::from_file_path(&path).map_err(|_| anyhow!("bad path"))?;
        let discoverer = Discoverer::new(ClockTime::SECOND)?;
        let res = tokio::join!(
            spawn_blocking(enc!((path) move || lofty::read_from_path(path))),
            async {
                spawn_blocking(enc!(
                    (uri) move || discoverer.discover_uri(uri.as_str()).map_err(anyhow::Error::from).map(Some)
                ))
                .await
            }
        );
        let metadata = res.0??;
        let info = res.1??;

        let mut s = DefaultHasher::new();
        path.hash(&mut s);
        let id = NonZeroU32::new((s.finish() as u32 % 244) + 1).unwrap();

        let mut this = Self {
            uri,
            metadata,
            image: None,
            path,
            image_cache: Default::default(),
            generated_image: true,
            id,
            info,
        };

        if let Some(tag) = this.metadata.primary_tag() {
            let picture = &tag
                .get_picture_type(PictureType::CoverFront)
                .or_else(|| tag.pictures().first());

            if let Some(picture) = picture {
                let format = ImageFormat::from_mime_type(
                    picture.mime_type().to_string(),
                );
                let mut img = ImageReader::new(Cursor::new(picture.data()));
                match format {
                    Some(format) => img.set_format(format),
                    None => img = img.with_guessed_format()?,
                }

                let img = img.decode().unwrap();
                this.image = Some(img);
                this.generated_image = false;
            }
        }

        Ok(this)
    }

    pub fn widget(&self, state: &TrackWidgetState<'_>) -> impl Widget {
        let tag = self.metadata.primary_tag();

        let style = state.style();

        let name = match tag {
            Some(tag) => tag.title(),
            None => None,
        };
        let mut name: String = match name {
            Some(x) => x.to_string(),
            None => self.uri.to_string(),
        };

        name.insert_str(0, state.name_prepender());

        Paragraph::label(name.with_style(style))
    }

    /// Creates a large widget of this track for use in the centre
    pub fn large_widget<'a>(
        &'a self,
        res_manager: &'a ResManager,
        config: &Config,
    ) -> impl Widget + 'a {
        let tag = self.metadata.primary_tag();

        let image = self.image(res_manager).map(|image| {
            // Don't use a filter for generated art
            let filter = match self.generated_image && res_manager.is_empty() {
                true => FilterType::Nearest,
                false => FilterType::Lanczos3,
            };
            KittyImage::new(image, self.id)
                .unwrap()
                .resize_filter(filter)
        });

        let mut skeletal_tags = SkeletalTags {
            image,
            ..Default::default()
        };

        if let Some(tag) = tag {
            skeletal_tags.title = tag.title();
            skeletal_tags.album = tag.album();
            skeletal_tags.artist = tag.artist();
        }

        create_large_widget(skeletal_tags, &self.image_cache, config)
    }

    pub fn image<'a>(
        &'a self,
        res_manager: &'a ResManager,
    ) -> Option<&'a DynamicImage> {
        self.image
            .as_ref()
            .or(res_manager.suggest_album_art(&self.path))
    }

    /// Generates automatic art for this
    pub fn generate_track_image(&mut self) -> anyhow::Result<()> {
        // Generate a picture from the audio file.

        // Opens the audio file and read in
        // some random data from it.
        let mut file = fs::File::open(&self.path)?;
        let len = file.metadata()?.len();
        file.seek(SeekFrom::Start(0x00001000.min(len)))?;

        let mut magic = [0; 14];
        file.read_exact(&mut magic)?;

        let col1 = img_color_from_bytes(magic[0..3].try_into()?)?;
        let col2 = img_color_from_bytes(magic[4..7].try_into()?)?;
        let between_col_1 = (magic[5] % 15 + 1) as usize;
        let between_col_2 = (magic[6] % 15 + 1) as usize;
        let row_size_1 = ((magic[7] / 64) % 4 + 1) as u32;
        let row_size_2 = ((magic[8] / 64) % 4 + 1) as u32;
        let len_col_1 = (magic[9] % 3 + 1) as usize * between_col_1 / 4;
        let len_col_2 = (magic[10] % 3 + 1) as usize * between_col_2 / 4;
        let start_1 = (magic[11] % 3) as usize;
        let start_2 = (magic[12] % 3) as usize;
        let rev_2 = (magic[13] % 2) == 1;

        // Creates an image
        let mut img = RgbImage::new(15, 15);
        let width = img.width();
        for (x, y, pixel) in img.enumerate_pixels_mut() {
            let y = if rev_2 { y / 2 + (1 - (y % 2)) } else { y };

            let n1 = (x + (y / row_size_1) * width) as usize;
            let n2 = (x + (y / row_size_2) * width) as usize;

            if (start_1 + n1) % between_col_1 < len_col_1 {
                *pixel = col1;
            }
            if (start_2 + n2) % between_col_2 < len_col_2 {
                *pixel = col2;
            }

            if *pixel == Rgb([0, 0, 0]) {
                *pixel = Rgb([0xFF, 0xFF, 0xFF]);
            }
        }

        self.image = Some(img.into());

        Ok(())
    }

    pub fn tags(&self) -> mpris::Tags {
        mpris::Tags::from_track(&self.metadata)
    }
}

fn img_color_from_bytes(b: [u8; 3]) -> Result<Rgb<u8>, anyhow::Error> {
    let rgb = Srgb::from(b);

    let rgb = rgb.into_linear::<f32>();
    let mut hsv = Hsv::try_from_color(rgb)?;
    hsv.saturation = 1.0;
    let rgb = palette::rgb::Rgb::from_color(hsv);
    let rgb: [u8; 3] = rgb.into_format().into();

    Ok(Rgb::from(rgb))
}
